import streamlit as st
from functions import *

todos = getTodos()

def addTodo():
    todo = st.session_state['newTodo'] + '\n'
    todos.append(todo)
    storeTodos(todos)
    st.session_state["newTodo"] = ""


st.title("My Todo App")
st.subheader("This is my todo app.")
st.write("This app is to increase you productivity")


for i, todo in enumerate(todos):
    checkbox = st.checkbox(todo, key=todo)
    if checkbox:
        todos.pop(i)
        storeTodos(todos)
        del st.session_state[todo]
        st.rerun()

st.text_input(label="", placeholder="Add new todo...",
              on_change=addTodo, key='newTodo')