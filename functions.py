FILEPATH = "todos.txt"

def getTodos(filepath=FILEPATH):
    with open(filepath, 'r') as file:
        tds = file.readlines()
    return tds


def storeTodos(tds, filepath=FILEPATH):
    with open(filepath, 'w') as file:
        file.writelines(tds)


if __name__ == "__main__":
    print(getTodos())
