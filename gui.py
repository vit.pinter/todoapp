from functions import *
import PySimpleGUI as sg
import time
import os

if not os.path.exists("todos.txt"):
    with open("todos.txt", 'w') as file:
        pass

sg.theme("SystemDefaultForReal")

clock = sg.Text('',key='clock')
label = sg.Text("Type in a to-do")
inputBox = sg.InputText(tooltip="Enter todo", key='todo')
addButton = sg.Button(image_source="add.png", key='Add', tooltip="Add", size=2)

listBox = sg.Listbox(values=getTodos(), key='todos',
                     enable_events=True, size=(45, 10))
editButton = sg.Button("Edit")
completeButton = sg.Button(image_source="complete.png",key="Complete", tooltip="Complete", size=2)
exitButton = sg.Button("Exit")

window = sg.Window('My To-Do App',
                   layout=[[clock],
                           [label],
                           [inputBox, addButton],
                           [listBox, editButton, completeButton],
                           [exitButton]],
                   font=('Helvetica', 13))

while True:
    event, values = window.read(timeout=500)
    window['clock'].update(value=time.strftime("%d.%-m.%Y %H:%M"))
    print(event)
    print(values)

    match event:
        case "Add":
            todos = getTodos()
            todos.append(values['todo'] + "\n")
            storeTodos(todos)
            window['todos'].update(values=todos)
            window['todo'].update(value='')

        case "Edit":
            try:
                todoToEdit = values['todos'][0]
                newTodo = values['todo']

                todos = getTodos()
                index = todos.index(todoToEdit)
                todos[index] = newTodo + '\n'
                storeTodos(todos)

                window['todos'].update(values=todos)
                window['todo'].update(value='')

            except IndexError:
                sg.popup("Error", "Please select an item first.")

        case 'Complete':
            try:
                todoToComplete = values['todos'][0]
                todos = getTodos()
                todos.remove(todoToComplete)
                storeTodos(todos)
                window['todos'].update(values=todos)
                window['todo'].update(value='')

            except IndexError:
                sg.popup("Error", "Please choose an item first.")

        case 'Exit':
            break

        case 'todos':
            window['todo'].update(value=values['todos'][0])

        case sg.WIN_CLOSED:
            break

window.close()
