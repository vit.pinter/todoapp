from functions import getTodos, storeTodos
from time import strftime
import os

if not os.path.exists("todos.txt"):
    with open("todos.txt", 'r') as file:
        pass

while True:
    print("Je " + strftime("%d.%-m.%Y %H:%M."))
    userAction = input("Type add, show, edit, complete or exit: ")
    userAction = userAction.strip()

    if userAction.startswith("add"):
        todo = userAction[4:] + '\n'

        todos = getTodos()
        todos.append(todo)
        storeTodos(todos)

    elif userAction.startswith("show"):
        todos = getTodos()

        for i, item in enumerate(todos):
            item = item.strip('\n')
            print(f"{i+1}-{item}")

    elif userAction.startswith("edit"):
        try:
            if userAction[5:] != '':
                number = int(userAction[5:])
            else:
                number = int(input("Number of the todo to edit: "))

            todos = getTodos()

            newTodo = input("Enter new todo: ")
            todos[number-1] = newTodo + '\n'

            storeTodos(todos)

        except ValueError:
            print('Your command is not valid. Number is expected after "Edit" command')
            continue

        except IndexError:
            print('No item with entered number')
            continue

    elif userAction.startswith("complete"):
        try:
            todos = getTodos()

            if userAction[9:] != '':
                number = int(userAction[9:])
            else:
                number = int(input("Number of the todo to complete: "))

            todoToRemove = todos[number-1].strip('\n')
            todos.pop(number-1)

            storeTodos(todos)

            print(f"Todo {todoToRemove} was removed from the list")

        except ValueError:
            print('Your command is not valid. Number is expected after "Complete" command')
            continue
        except IndexError:
            print('No item with entered number')
            continue

    elif userAction.startswith("exit"):
        break

    else:
        print("Invalid command")

print("Bye!")
